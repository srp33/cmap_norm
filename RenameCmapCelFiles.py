import os, sys, glob, shutil, uuid
import xlrd

mapFilePath = sys.argv[1]
inDirPath = sys.argv[2]
queryDrugName = sys.argv[3]
queryCellLine = sys.argv[4]
queryArrayType = sys.argv[5]
outDirPath = sys.argv[6]

if not inDirPath.endswith("/"):
    inDirPath += "/"

if not outDirPath.endswith("/"):
    outDirPath += "/"

book = xlrd.open_workbook(mapFilePath)
sheet = book.sheet_by_index(0)
rawData = [sheet.row_values(i) for i in range(sheet.nrows)]
header = rawData.pop(0)

perturbationFilePathDict = {}
vehicleFilePathDict = {}

rowCount = 0
for row in rawData:
    rowCount += 1

    if len(str(row[-1])) == 0:
        continue

    instanceID = str(int(row[0]))
    batchID = str(row[1]).replace(".0", "")
    drugName = str(row[2]).replace("/","").replace(" ", "_")
    concentration = ("%.10f" % row[4]).rstrip("0")
    cellLine = str(row[6])
    arrayType = str(row[7])
    perturbationScanID = str(row[8]).replace('\'', '')

    if queryDrugName not in ("*", drugName) or queryCellLine not in ("*", cellLine) or queryArrayType not in ("*", arrayType):
        continue

    vehicleScanIDSuffixes = str(row[9])
    if vehicleScanIDSuffixes.find(".") > -1:
        vehicleScanIDSuffixes = vehicleScanIDSuffixes[1:].split(".")
    else:
        vehicleScanIDSuffixes = [vehicleScanIDSuffixes]

    inPerturbationFilePath = inDirPath + perturbationScanID + ".CEL.bz2"
    outPerturbationFilePath = outDirPath + "Perturbed__%s__%s__%s__%s__Batch%s__%s__%s.CEL.bz2" % (instanceID, drugName, concentration, cellLine, batchID, arrayType, perturbationScanID)

    perturbationFilePathDict[inPerturbationFilePath] = outPerturbationFilePath

    for vehicleScanIDSuffix in vehicleScanIDSuffixes:
        inVehicleFilePath = inDirPath + vehicleScanIDSuffix + ".CEL.bz2"

        if not os.path.exists(inVehicleFilePath):
            inVehicleFilePath = inPerturbationFilePath.split(".")[0] + "." + inVehicleFilePath.replace(inDirPath, "")

        outVehicleFilePath = outDirPath + "Vehicle__%s__Batch%s__%s__%s" % (cellLine, batchID, arrayType, os.path.basename(inVehicleFilePath))
        vehicleFilePathDict[inVehicleFilePath] = outVehicleFilePath

def copyDictFiles(filePathDict):
    for inFilePath in filePathDict:
        outFilePath = filePathDict[inFilePath]

        if os.path.exists(inFilePath) and not os.path.exists(outFilePath):
            print "Copying %s to %s" % (inFilePath, outFilePath)
            shutil.copy(inFilePath, outFilePath)

copyDictFiles(perturbationFilePathDict)
copyDictFiles(vehicleFilePathDict)
