import os, sys, glob, shutil, uuid, gzip
import xlrd

mapFilePath = sys.argv[1]
inDirPath = sys.argv[2]
queryDrugName = sys.argv[3]
queryCellLine = sys.argv[4]
queryArrayType = sys.argv[5]
queryConcentration = sys.argv[6]
outVehicleFilePath = sys.argv[7]
outPerturbedFilePath = sys.argv[8]

book = xlrd.open_workbook(mapFilePath)
sheet = book.sheet_by_index(0)
rawData = [sheet.row_values(i) for i in range(sheet.nrows)]
header = rawData.pop(0)

perturbationFilePaths = set()
vehicleFilePaths = set()

for row in rawData:
    if len(str(row[-1])) == 0:
        continue

    instanceID = str(int(row[0]))
    batchID = str(row[1]).replace(".0", "")
    drugName = str(row[2]).replace("/","").replace(" ", "_")
    concentration = ("%.10f" % row[4]).rstrip("0")
    cellLine = str(row[6])
    arrayType = str(row[7])
    perturbationScanID = str(row[8]).replace('\'', '')

    if queryDrugName not in ("*", drugName) or queryCellLine not in ("*", cellLine) or queryArrayType not in ("*", arrayType) or queryConcentration not in ("*", concentration):
        continue

    vehicleScanIDSuffixes = str(row[9])
    if vehicleScanIDSuffixes.startswith("."):
        vehicleScanIDSuffixes = vehicleScanIDSuffixes[1:].split(".")
    else:
        vehicleScanIDSuffixes = [vehicleScanIDSuffixes.replace("'", "")]

    perturbationFilePath = inDirPath + "/Perturbed__%s__%s__%s__%s__Batch%s__%s__%s.gz" % (instanceID, drugName, concentration, cellLine, batchID, arrayType, perturbationScanID)

    if not os.path.exists(perturbationFilePath):
        print "No file exists at %s" % perturbationFilePath
        sys.exit(1)

    perturbationFilePaths.add(perturbationFilePath)

    for vehicleScanIDSuffix in vehicleScanIDSuffixes:
        vehicleFilePath = inDirPath + "/Vehicle__%s__Batch%s__%s__%s.gz" % (cellLine, batchID, arrayType, vehicleScanIDSuffix)
        vehicleFilePatha = inDirPath + "/Vehicle__%s__Batch%sa__%s__%s.gz" % (cellLine, batchID, arrayType, vehicleScanIDSuffix)
        vehicleFilePathb = inDirPath + "/Vehicle__%s__Batch%sb__%s__%s.gz" % (cellLine, batchID, arrayType, vehicleScanIDSuffix)

        if os.path.exists(vehicleFilePath):
            vehicleFilePaths.add(vehicleFilePath)
        elif os.path.exists(vehicleFilePatha):
            vehicleFilePaths.add(vehicleFilePatha)
        elif os.path.exists(vehicleFilePathb):
            vehicleFilePaths.add(vehicleFilePathb)
        else:
            vehicleFilePath2 = inDirPath + "/Vehicle__%s__Batch%s__%s__%s.%s.gz" % (cellLine, batchID, arrayType, perturbationScanID.split(".")[0], vehicleScanIDSuffix)

            if not os.path.exists(vehicleFilePath2):
                print "No file exists at %s" % vehicleFilePath2
                sys.exit(1)

            vehicleFilePaths.add(vehicleFilePath2)

dataDict = {}
vehicleSampleIDs = set()
perturbedSampleIDs = set()

for filePath in list(vehicleFilePaths) + list(perturbationFilePaths):
    dataFile = gzip.open(filePath)
    sampleID = dataFile.readline().rstrip().split("\t")[1]

    if os.path.basename(filePath).startswith("Vehicle__"):
        vehicleSampleIDs.add(sampleID)
    if os.path.basename(filePath).startswith("Perturbed__"):
        perturbedSampleIDs.add(sampleID)

    for line in dataFile:
        lineItems = line.rstrip().split("\t")
        feature = lineItems[0]
        value = lineItems[1]

        if not feature in dataDict:
            dataDict[feature] = {}

        dataDict[feature][sampleID] = value

    dataFile.close()

vehicleSampleIDs = sorted(list(vehicleSampleIDs))
perturbedSampleIDs = sorted(list(perturbedSampleIDs))
sampleIDs = vehicleSampleIDs + perturbedSampleIDs

features = []
for feature in sorted(dataDict.keys()):
    if len(dataDict[feature]) == len(sampleIDs):
        features.append(feature)

#####################################
#features = features[:10]
#####################################

def writeOutputFile(outFilePath, sampleIDs):
    outFile = open(outFilePath, 'w')
    outFile.write("\t".join([""] + features  + ["Class"]) + "\n")
    for sampleID in sampleIDs:
        sampleClass = sampleID.split("__")[0]
        values = [dataDict[feature][sampleID] for feature in features]
        outFile.write("\t".join([sampleID] + values + [sampleClass]) + "\n")
    outFile.close()

print "%i vehicle samples: %s" % (len(vehicleSampleIDs), ",".join(vehicleSampleIDs))
print "%i perturbed samples: %s" % (len(perturbedSampleIDs), ",".join(perturbedSampleIDs))

writeOutputFile(outVehicleFilePath, vehicleSampleIDs)
writeOutputFile(outPerturbedFilePath, perturbedSampleIDs)
